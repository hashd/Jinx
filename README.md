Jinx
====

This is a CLI to work with Github.

```
$ ./jinx help
usage: jinx (switch) [args]
jinx [--repos/-r] Imaginea (count|10)
jinx [--issues/-i] Imaginea KodeBeagle (count|10)
jinx [--collaborators/-c] Imaginea KodeBeagle (count|10)
jinx [--teams/-T] Imaginea KodeBeagle (count|10)
jinx [--tags/-t] Imaginea KodeBeagle (count|10)
jinx [--search_repos/-sr] spark (sort_order)
```
