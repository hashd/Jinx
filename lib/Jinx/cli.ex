defmodule Jinx.CLI do
  import Jinx.Util.TableFormatter, only: [print_table_for_columns: 2]
  import Jinx.Util.HTTP
  require Logger

  @default_count 10
  @moduledoc """
  CLI Input Handler
  """

  def main(argv) do
    argv |> parse_args |> process
  end

  def parse_args(argv) do
    parse = OptionParser.parse(
      argv, 
      switches: [help: :boolean, 
        repos: :boolean, 
        issues: :boolean, 
        collaborators: :boolean, 
        languages: :boolean, 
        tags: :boolean, 
        teams: :boolean,
        search_repos: :boolean,
        search_code: :boolean
      ], 
      aliases: [h: :help, 
        r: :repos, 
        i: :issues, 
        c: :collaborators, 
        l: :languages, 
        t: :tags, 
        T: :teams,
        sr: :search_repos,
        sc: :search_code
      ]
    );

    case parse do
      { [help: true], _, _ } -> :help
      { [repos: true], [user, count], _ } -> {:repos, {:user, user, count |> String.to_integer}}
      { [repos: true], [user], _ } -> {:repos, {:user, user, @default_count}}
      { [issues: true], [user, project, count], _} -> {:issues, { user, project, count |> String.to_integer }}
      { [issues: true], [user, project], _} -> {:issues, { user, project, @default_count}}
      { [collaborators: true], [user, project, count], _} -> {:collab, {user, project, count}}
      { [collaborators: true], [user, project], _} -> {:collab, {user, project, @default_count}}
      { [languages: true], [user, project], _} -> {:lang, {user, project}}
      { [teams: true], [user, project, count], _} -> {:teams, {user, project, count |> String.to_integer}}
      { [teams: true], [user, project], _} -> {:teams, {user, project, @default_count}}
      { [tags: true], [user, project, count], _} -> {:tags, {user, project, count |> String.to_integer}}
      { [tags: true], [user, project], _} -> {:tags, {user, project, @default_count}}
      { [search_repos: true], [query], _} -> {:search_repos, {query}}
      { [search_repos: true], [query, sort_criteria], _} -> {:search_repos, {query, sort_criteria}}
      { [search_code: true], [query], _} -> {:search_code, {query}}
      { [search_code: true], [query, sort_criteria], _} -> {:search_code, {query, sort_criteria}}
      _ -> :help
    end
  end

  def process(:help) do
    IO.puts """
    usage: jinx (switch) [args]
    jinx [--repos/-r] Imaginea (count|10)
    jinx [--issues/-i] Imaginea KodeBeagle (count|10)
    jinx [--collaborators/-c] Imaginea KodeBeagle (count|10)
    jinx [--teams/-T] Imaginea KodeBeagle (count|10)
    jinx [--tags/-t] Imaginea KodeBeagle (count|10)
    jinx [--search_repos/-sr] spark (sort_order)
    """
    System.halt 0
  end

  def process({:issues, {user, project, count}}) do
    Jinx.Github.Issues.fetch(user, project) |> 
      decode_response |> 
      convert_to_list_of_hashdicts |>
      sort_into_ascending_order |>
      Enum.take(count) |>
      print_table_for_columns(["number", "created_at", "title"])
  end

  def process({:repos, {:user, user, count}}) do
    Jinx.Github.Repos.fetch_repos(user) |>
      decode_response |> 
      convert_to_list_of_hashdicts |>
      sort_into_ascending_order |>
      Enum.take(count) |>
      print_table_for_columns(["name", "html_url", "description", "forks_count", "stargazers_count", "created_at"])
  end

  def process({:collab, {user, project, count}}) do
    Jinx.Github.Repos.fetch_contributors(user, project) |>
      decode_response |> 
      convert_to_list_of_hashdicts |>
      sort_into_ascending_order |>
      Enum.take(count) |>
      print_table_for_columns(["login", "html_url", "type", "site_admin", "contributions"])
  end

  def process({:lang, {user, project}}) do
    Jinx.Github.Repos.fetch_languages(user, project) |>
      decode_response |> 
      convert_to_hashdict |>
      IO.inspect |>
      print_table_for_columns(["login", "html_url", "type", "site_admin", "contributions"])
  end

  def process({:teams, {user, project, count}}) do
    Jinx.Github.Repos.fetch_teams(user, project) |>
      decode_response |> 
      convert_to_list_of_hashdicts |>
      sort_into_ascending_order |>
      Enum.take(count) |>
      print_table_for_columns(["name", "slug", "description", "permission", "members_url"])
  end

  def process({:tags, {user, project, count}}) do
    Jinx.Github.Repos.fetch_tags(user, project) |>
      decode_response |> 
      convert_to_list_of_hashdicts |>
      sort_into_ascending_order |>
      Enum.take(count) |>
      print_table_for_columns(["name", "commit", "zipball_url", "tarball_url"])
  end

  def process({:search_repos, {query}}) do
    Jinx.Github.Search.search_repos(query) |>
      decode_response |> 
      Map.get("items") |>
      convert_to_list_of_hashdicts |>
      print_table_for_columns(["full_name", "language", "open_issues_count", "forks", "stargazers_count", "created_at", "updated_at"])
  end

  def process({:search_repos, {query, sort_criteria}}) do
    Jinx.Github.Search.search_repos(query, sort_criteria) |>
      decode_response |> 
      Map.get("items") |>
      convert_to_list_of_hashdicts |>
      print_table_for_columns(["full_name", "language", "open_issues_count", "forks", "stargazers_count", "created_at", "updated_at"])
  end

  def process({:search_code, {query}}) do
    Jinx.Github.Search.search_code(query) |>
      decode_response |> 
      Map.get("items") |>
      convert_to_list_of_hashdicts |>
      print_table_for_columns(["full_name", "language", "open_issues_count", "forks", "stargazers_count", "created_at", "updated_at"])
  end

  def process({:search_code, {query, sort_criteria}}) do
    Jinx.Github.Search.search_code(query, sort_criteria) |>
      decode_response |> 
      Map.get("items") |>
      convert_to_list_of_hashdicts |>
      print_table_for_columns(["full_name", "language", "open_issues_count", "forks", "stargazers_count", "created_at", "updated_at"])
  end

  def convert_to_hashdict(map) do
    map |> Enum.into(HashDict.new)
  end

  def convert_to_list_of_hashdicts(list) do
    list |> Enum.map(&Enum.into(&1, HashDict.new))
  end

  def sort_into_ascending_order(list, criteria \\ "created_at") do
    Enum.sort list, fn i1, i2 -> i1[criteria] >= i2[criteria] end
  end
end