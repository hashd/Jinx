defmodule Jinx.Github.Search do
  import Jinx.Util.HTTP;
  require Logger

  @user_agent [ {"User-agent", "Github client"}]
  @github_url Application.get_env(:jinx, :github_url)

  def search_repos(query) do
    search_default_repo_url(query) |> HTTPoison.get(@user_agent) |> handle_response
  end

  def search_repos(query, sort_criteria) do
    search_repo_url(query, sort_criteria) |> HTTPoison.get(@user_agent) |> handle_response
  end

  def search_code(query) do
    search_default_code_url(query) |> HTTPoison.get(@user_agent) |> handle_response
  end

  def search_code(query, sort_criteria) do
    search_code_url(query, sort_criteria) |> IO.inspect |> HTTPoison.get(@user_agent) |> handle_response
  end

  def search_default_repo_url(query) do
    "#{@github_url}/search/repositories?q=#{query}"
  end

  def search_repo_url(query, sort_criteria) do
    "#{@github_url}/search/repositories?q=#{query}&sort=#{sort_criteria}"
  end

  def search_default_code_url(query) do
    "#{@github_url}/search/code?q=#{query}"
  end

  def search_code_url(query, sort_criteria) do
    "#{@github_url}/search/code?q=#{query}&sort=#{sort_criteria}"
  end
end