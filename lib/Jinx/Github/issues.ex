defmodule Jinx.Github.Issues do
  import Jinx.Util.HTTP;
  require Logger

  @user_agent [ {"User-agent", "Github client"}]
  @github_url Application.get_env(:jinx, :github_url)

  def fetch(user, project) do
    issues_url(user, project) |> HTTPoison.get(@user_agent) |> handle_response
  end

  def issues_url(user, project) do
    "#{@github_url}/repos/#{user}/#{project}/issues"
  end
end