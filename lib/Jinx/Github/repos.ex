defmodule Jinx.Github.Repos do
  import Jinx.Util.HTTP;
  require Logger

  @user_agent [ {"User-agent", "Github client"}]
  @github_url Application.get_env(:jinx, :github_url)

  def fetch_repos(user) do
    repos_url(:user, user) |> HTTPoison.get(@user_agent) |> handle_response
  end

  def fetch_contributors(user, project) do
    contributors_url(user, project) |> HTTPoison.get(@user_agent) |> handle_response
  end

  def fetch_languages(user, project) do
    Logger.info "Fetching languages for #{user}::#{project}"
    languages_url(user, project) |> HTTPoison.get(@user_agent) |> handle_response
  end

  def fetch_teams(user, project) do
    Logger.info "Fetching teams for #{user}::#{project}"
    teams_url(user, project) |> HTTPoison.get(@user_agent) |> handle_response
  end

  def fetch_tags(user, project) do
    tags_url(user, project) |> HTTPoison.get(@user_agent) |> handle_response
  end

  def repos_url(:user, user) do
    "#{@github_url}/users/#{user}/repos"
  end

  def repos_url(:org, org) do
    "#{@github_url}/orgs/#{org}/repos"
  end

  def contributors_url(user, project) do
    "#{@github_url}/repos/#{user}/#{project}/contributors"
  end

  def languages_url(user, project) do
    "#{@github_url}/repos/#{user}/#{project}/languages"
  end

  def teams_url(user, project) do
    "#{@github_url}/repos/#{user}/#{project}/teams"
  end

  def tags_url(user, project) do
    "#{@github_url}/repos/#{user}/#{project}/tags"
  end
end