defmodule Jinx.Util.HTTP do
  require Logger

  def handle_response({:ok, %{status_code: 200, body: body}}) do
    {:ok, body |> :jsx.decode }
  end
  
  def handle_response({:ok, %{status_code: status, body: body}}) do
    Logger.error "Error #{status} returned"
    {:error, body |> :jsx.decode }
  end

  def decode_response({:ok, body}), do: body
  def decode_response({:error, error}) do
    {_, message} = error |> List.keyfind("message", 0)
    IO.puts "Error fetching issues from Github: #{message}"
    System.halt 0
  end
end